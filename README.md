# USB Flash drive for extra? KeepassXC/SSH safety

### Portable SSH keys
Flash drive is encrypted
SSH key password is stored in KeePassXC database
Password to unlock database is GPG encrypted on encrypted drive using another password  


## notes
Encrypting drives, generating a random password for KeepassXC database etc. should be done using a live CD on a disconnected computer.  
`cat /proc/sys/kernel/random/entropy_avail` to check available entropy  

1. Encrypt keyfile drive using a strong password

2. Use password generator to make a _**CRAZY**_ password for KeepassXC database

3. Encrypt password file with a strong password that is not easy to bruteforce or use a wordlist on, delete unencrypted password

`gpg -c --output encrypted_file plain_password_file && rm plain_password_file`

4. Move keyfile and encrypted password to encrypted drive '.keepassxc' folder

5. Generate SSH keys using script in .scripts


Easy integration with KeePassXC because of [python-keepassxc-browser](https://github.com/hrehfeld/python-keepassxc-browser)

