#!/usr/bin/env python3
# Serve HTML page with self signed certificate \
# that accesses KeePassXC through browser extension?
# U2F?
# Interface for managing SSH keys

import os
import subprocess
from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qsl
# no certificates while testing
#import ssl

class HandleRequests(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write('<html> \
        <head> \
        <title>KeePassXC Unlock</title> \
        </head> \
        <body> \
        <form method="post"> \
        <input action="/" type="hidden" name="unlock" value="ja"><br> \
        Pass: <input type="text" name="unlock_pass"><br> \
        <input type="submit" value="Submit"> \
        </form> \
        </body> \
        </html>'.encode())

    def do_POST(self):
        self._set_headers()
        content_len = int(self.headers.get('content-length', 0))
        post_body = self.rfile.read(content_len)
        content_length = int(self.headers['Content-Length'])
        self.do_SOMETHING(post_body)

    def do_PUT(self):
        self.do_POST()

    def do_SOMETHING(self, post_request):
        flash_path = os.getcwd()
        request = parse_qsl(urlparse("?{}".format(post_request.decode('utf-8'))).query)
        action = {}
        # build dictionary from POST request
        for count in range(0, len(request)):
            action[request[count][0]] = request[count][1]
        do_unlock = action['unlock']
        if do_unlock == 'ja':
            unlock_pass = action['unlock_pass']
            subprocess.call([flash_path + '/autorun', unlock_pass])


httpd = HTTPServer(('127.0.0.1', 8888), HandleRequests)
#httpd.socket = ssl.wrap_socket (httpd.socket, certfile='example.crt', keyfile='example.key', server_side=True)
httpd.serve_forever()
