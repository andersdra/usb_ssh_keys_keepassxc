#!/bin/sh
# set custom KeePassXC settings

keepass_config="$HOME/.config/keepassxc/keepassxc.ini"

edit_setting()
{
sed -i "s/$1/$2/g" $keepass_config
}

# allow more than one KeePass window at a time
edit_setting 'SingleInstance=true' 'SingleInstance=false'
# minimize on startup, not working
#edit_setting 'MinimizeOnStartup=false' 'MinimizeOnStartup=true'

