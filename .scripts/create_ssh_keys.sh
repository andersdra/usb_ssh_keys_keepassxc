#!/bin/bash
# create SSH keys for USB storage
# TODO fix permissions for key folders

script_path="$(dirname "$(realpath "$0")")"
PATH='/usr/bin'

if [ -z "$1" ]
then
  echo "./$0 passes key_comment"
  exit 1
fi

: "${SSH_ADD_KEY_ID=0}"
: "${HOSTNAME=$(uname -r)}"

ssh_host_dir="$script_path/../.hosts/.$HOSTNAME"

if [ ! -d "$ssh_host_dir" ]
then
  mkdir --parents "$ssh_host_dir"
fi

# check if key ID already exists
if [ -d "$ssh_host_dir/.$SSH_ADD_KEY_ID" ]
then
  echo "Key with ID $SSH_ADD_KEY_ID already exists for $HOSTNAME"
  exit 1
fi

ssh_directory="$ssh_host_dir/.$SSH_ADD_KEY_ID/.ssh"

# create folder for key
mkdir --parents "$ssh_directory"

# create key, set permissions
echo 'Key password? Leave empty for generation of a random password' \
&& read -r key_password

if [ -z "$key_password" ]
then
  echo 'generating random password'
  key_password='random'
fi

ssh-keygen -a "$1" \
-N "$key_password" \
-t ed25519 \
-f "$ssh_directory/id_ed25519" \
-C "$2" \
-V "-1m:forever" > /dev/null \
&& echo 'Key name?' \
&& read -r key_name \
&& echo 'Enter key note' \
&& read -r key_note \
&& echo "$key_name" > "$ssh_directory/.key_$SSH_ADD_KEY_ID.note" \
&& echo "$key_note" > "$ssh_directory/.key_$SSH_ADD_KEY_ID.note" \
&& echo 'How many seconds do you want the key to be loaded to memory by default?' \
&& read -r key_time_in_memory \
&& echo "seconds_in_memory=$key_time_in_memory" > "$ssh_directory/.key_$SSH_ADD_KEY_ID.settings" \
&& chmod 700 "$ssh_directory/id_ed25519" \
&& chmod 744 "$ssh_directory/id_ed25519.pub" \
&& "$script_path"/../env/bin/python3 "$script_path"/keepass_add.py "$key_password" "$SSH_ADD_KEY_ID"
